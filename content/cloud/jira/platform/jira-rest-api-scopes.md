---
title: JIRA Cloud platform REST API scopes
platform: cloud
product: jiracloud
category: reference
subcategory: api
aliases:
- /jiracloud/jira-rest-api-scopes.html
- /jiracloud/jira-rest-api-scopes.md
date: "2016-10-04"
---

# JIRA Cloud platform REST API scopes

{{< include path="docs/content/cloud/connect/reference/product-api-scopes.snippet.md" >}}

{{< include path="docs/content/cloud/jira/platform/temp/jira-rest-api-scopes-reference.snippet.md" >}}
