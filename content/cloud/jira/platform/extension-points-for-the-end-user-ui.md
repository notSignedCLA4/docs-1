---
title: "Extension points for the end-user UI" 
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.html
- /jiracloud/jira-platform-modules-user-accessible-locations-39988374.md
confluence_id: 39988374
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988374
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988374
date: "2016-09-30"
---
#  Extension points for the end-user UI 

This pages lists the extension points for modules for locations in the JIRA UI that are accessible by non-admin users.

## Top navigation bar location

Defines web items in JIRA's top navigation bar, which are accessible from all JIRA areas (except JIRA's administration area/mode).

#### Module type
`webItem`

#### Screenshot
<img src="/cloud/jira/platform/images/jdev-header.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.top.navigation.bar",
            "name": { "value": "Example add-on link" },
            "url": "http://www.example.com",
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location to `system.top.navigation.bar`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The target URL for the navigation bar item.

----

## User name drop-down location

Defines web sections and items in JIRA's user name and help drop-down menus, which are accessible from all JIRA screens.

#### Module type
`webSection` + `webItem`

#### Screenshot
<img src="/cloud/jira/platform/images/jdev-userprofile.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "system.user.options",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.user.options/personal",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-section-link
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the webSection, set the location to `system.user.options`. For each webPanel, set the location to the key of the webSection.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  Target URL for the menu item.

----

## User profile page dropdown location

Defines web items of the more (...) dropdown menu on a JIRA user's user profile page. This location has only one section (`operations`) to which custom web items can be added.

#### Module type
`webItem`

#### Screenshot
<img src="/cloud/jira/platform/images/jdev-userprofilepagedropdown-location.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.user.profile.links/operations",
            "name": {
                "value": "Example add-on link"
            },
            "url": "/example-link"
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: Set the location to `system.user.profile.links/operations`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The target URL for the menu item.

----

## Hover profile links location

Defines web items in JIRA's hover profile feature, which is accessible when a user hovers their mouse pointer over a JIRA user's name throughout JIRA's user interface.

#### Module type
`webSection` + `webItem`

#### Screenshot
<img src="/cloud/jira/platform/images/jdev-userprofilehover-location.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "system.user.hover.links",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "example-menu-section",
            "name": {
                "value": "Example add-on link"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the webSection, set the location to `system.user.options`. For each webPanel, set the location to the key of the webSection.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

----

## Dialog box hint location

Defines web items that allow you to add hints on JIRA's dialog boxes. You can add hints to most JIRA dialog boxes. To add your own web item to JIRA's dialog box hints location for a specific dialog box, your web item must include a section attribute with the value `jira.hints/LOCATION_CONTEXT`. The `LOCATION_CONTEXT` is a predefined 'context' in JIRA that determines on which dialog box your hints will appear:

-   `TRANSITION` -- Hints on a 'transition issue' dialog box.
-   `ASSIGN` -- Hints on the 'Assign' dialog box.
-   `LABELS` -- Hints on the 'Labels' dialog box.
-   `COMMENT` -- Hints on 'Comment' dialog boxes.
-   `CLONE` -- Hints on 'Clone Issue' dialog boxes.
-   `DELETE_FILTER` -- Hints on 'Delete Filter' dialog boxes.
-   `ATTACH` -- Hints on 'Attach Files' dialog boxes, not the 'Attach Screenshot' dialog.
-   `DELETE_ISSUE` -- Hints on 'Delete issue' dialog boxes.
-   `LINK` -- Hints on 'Link issue' dialog boxes.
-   `LOG_WORK` -- Hints on 'Log work' dialog boxes.

#### Module type
`webSection` + `webItem`

#### Screenshot
<img src="/cloud/jira/platform/images/jdev-dialogboxhint-location.png"/>

#### Sample JSON
``` json
...
"modules": {
    "webSections": [
        {
            "key": "example-menu-section",
            "location": "jira.hints/ASSIGN",
            "name": {
                "value": "Example add-on name"
            }
        }
    ],
    "webItems": [
        {
            "key": "example-section-link",
            "location": "example-menu-section",
            "name": {
                "value": "Example add-on link"
            }
        }
    ]
}
...
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`location`

-   **Description**: For the `webSection`, set the location to `jira.hints/` + `LOCATION_CONTEXT`, as described above. For each `webPanel`, set the location to the key of the `webSection`.

`name`

-   **Type**: [i18n property]
-   **Required**: yes
-   **Description**: A human readable name. 

  [i18n property]: /cloud/jira/platform/connect/modules/i18n-property
  [additional context]: /cloud/jira/platform/context-parameters