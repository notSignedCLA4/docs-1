---
title: JIRA Software Context Parameters 
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-software-context-parameters-39990789.html
- /jiracloud/jira-software-context-parameters-39990789.md
confluence_id: 39990789
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990789
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990789
date: "2016-05-20"
---
# JIRA Software context parameters

Context parameters are special, context-specific values that can be supplied to your add-ons. 
These values are provided via special tokens, which are inserted into the URL properties of 
your [JIRA Software modules], specifically panels and actions.

Using context parameters, your Atlassian Connect add-ons can selectively alter their behavior 
based on information provided by JIRA Software. Common examples include displaying alternate 
content, or even performing entirely different operations based on the available context.

## Using parameters

Context parameters can be used with any UI module that takes a URL property, as specified in your 
`atlassian-connect.json` descriptor. To use a context parameter, simply insert the corresponding 
key name, surrounded by curly-braces, at any location within your URL. The parameter will then be 
substituted into the URL as JIRA Software renders your UI module.

For example, the URL in the following snippet includes the `board.id` and `board.type` parameters:

``` java
...
"modules": {      
    "webPanels": [
        {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "location": "jira.agile.board.configuration",
            "name": {
                "value": "My board configuration page"
            },
            "weight": 1
        }
    ]
}
...
```

## JIRA Software parameters

The following table details the list of context parameters provided by JIRA Software:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th>Parameter key</th>
<th>Description</th>
<th>Available for</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>{{< highlight javascript >}}board.id{{< /highlight >}}</td>
<td>The ID of the current board.</td>
<td><p> </p></td>
</tr>
<tr class="even">
<td>{{< highlight javascript >}}board.type{{< /highlight >}}</td>
<td>The type (e.g. scrum, kanban) of the current request type.</td>
<td><p> </p></td>
</tr>
<tr class="odd">
<td>{{< highlight javascript >}}board.screen{{< /highlight >}}</td>
<td>The screen of the current board.<br />
<em>Note, the old `board.mode` parameter was deprecated in favor of this parameter.</em></td>
<td><p>Modules that are displayed in multiple board screens.</p></td>
</tr>
<tr class="even">
<td>`sprint.id`</td>
<td>The ID of the current sprint.</td>
<td> </td>
</tr>
<tr class="odd">
<td>`sprint.state`</td>
<td>The state of the current sprint.</td>
<td> </td>
</tr>
</tbody>
</table>

## Additional parameters

The JIRA platform provides a number of additional context parameters that JIRA Software add-ons can use. For a full list of these context parameters, see the [Atlassian Connect documentation].

  [JIRA Software modules]: /cloud/jira/software/jira-software-modules
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html
