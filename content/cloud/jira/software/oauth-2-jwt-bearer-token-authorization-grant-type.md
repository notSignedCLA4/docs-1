---
title: "OAuth 2.0 - JWT Bearer token authorization grant type"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2016-11-02"
---
{{< include path="docs/content/cloud/connect/concepts/OAuth2-JWT-Bearer-Token-Authentication.snippet.md">}}