

<table class='aui aui-table-sortable'>
   
    <tbody>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-sequence-diagramr">Sequence Diagramr</a> (node.js)</td>
            <td>A Confluence macro for creating UML sequence diagrams on the page.</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/atlassianlabs/atlassian-connect-confluence-word-cloud">Tim's Word Cloud</a> (node.js)</td>
            <td>A macro that takes the contents of a page and constructs an SVG based word cloud.</td>
        </tr>
        
        
        <tr>
            <td><a href="https://bitbucket.org/acgmaps/acgmaps.bitbucket.org">Google Maps macro</a> (static)</td>
            <td>Insert Google Maps into your Confluence pages</td>
        </tr>
        <tr>
            <td><a href="https://bitbucket.org/robertmassaioli/sketchfab-connect">Sketchfab for Confluence</a> (static)</td>
            <td>A static Atlassian Connect add-on that renders Sketchfab 3D models in Confluence pages</td>
        </tr>

    </tbody>
</table>

