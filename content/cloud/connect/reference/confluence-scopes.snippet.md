# Scopes

Scopes allow an add-on to request a particular level of access to an Atlassian product.

* Within a particular product instance, an administrator may further limit add-on actions, allowing administrators to safely install add-ons they otherwise would not.
* The scopes may allow the *potential* to access beta or non-public APIs that are later changed in or removed from the
Atlassian product. The inclusion of the API endpoint in a scope does not imply that the product makes this endpoint
public. Read the [Confluence API documentation](../confluence-rest-api/) for details.

The following scopes are available for use by Atlassian Connect Confluence add-ons:

* `NONE` &ndash; can access add-on defined data - this scope does not need to be declared in the descriptor.
* `READ` &ndash; can view, browse, read information from Confluence
* `WRITE` &ndash; can create or edit content in Confluence, but not delete them
* `DELETE` &ndash; can delete entities from Confluence
* `SPACE_ADMIN` &ndash; can administer a space in Confluence
* `ADMIN` &ndash; can administer the entire Confluence instance
* `ACT_AS_USER` &ndash; can enact services on a user's behalf.



### Example

Scopes are declared as a top level attribute of [`atlassian-connect.json` add-on descriptor](../add-on-descriptor/) as in this example:
``` json
    {
        "baseUrl": "http://my-addon.com",
        "key": "atlassian-connect-addon",
        "scopes": [
            "read", "write"
        ],
        "modules": {}
    }
```
### Reference

See [Confluence scopes reference](../confluence-rest-api-scopes/) for specific resources and related scopes.