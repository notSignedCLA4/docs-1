# Getting help

Atlassian Connect is under active development and there's an active community of developers building Connect add-ons. There are lots of ways to get answers to your questions!

## [The Atlassian Developer Community](https://community.developer.atlassian.com/t/welcome-to-the-community/84)

All developers working with Atlassian Connect are encouraged to join the [Atlassian Developer Community](https://community.developer.atlassian.com/t/welcome-to-the-community/84)). Both Atlassian developers and community developers are active on this community.


## [Atlassian Connect JIRA project](https://ecosystem.atlassian.net/browse/AC/)

Submit new feature requests, bugs, and feature votes for the Connect add-on framework at [https://ecosystem.atlassian.net/browse/AC/](https://ecosystem.atlassian.net/browse/AC/).


## [Help with the Atlassian Marketplace](https://marketplace.atlassian.com/)

If you have questions or need help listing your new Atlassian Connect add-on for sale on the Atlassian Marketplace, you
can ask questions on the [Atlassian Developer Community](https://community.developer.atlassian.com/c/marketplace-vendors)
or file issues in the [Atlassian Marketplace Issue tracker](https://ecosystem.atlassian.net/browse/AMKT/)


